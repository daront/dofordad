<?php

    include 'connect.php';
    
    $pixel = $_POST['pixel'];
    
    $sql="SELECT fb_id, name, goal FROM user WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $pixel);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($fb_id, $name, $goal);
    $result = array();
    if ($stmt->num_rows > 0) {
        $stmt->fetch();
        $result['fb_id'] = $fb_id;
        $result['name'] = $name;
        $result['goal'] = $goal;
    } 
    
    echo json_encode($result);
?>