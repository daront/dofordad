<?php

    include 'connect.php';
    
    $fb_id = $_POST['fb_id'];
    
    $sql="SELECT id FROM user WHERE fb_id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param('i', $fb_id);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($id);
    $result = array();
    if ($stmt->num_rows > 0) {
        $stmt->fetch();
        $result['id'] = $id;
        
    } 
    
    echo json_encode($result);
?>