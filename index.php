<?php

    include 'connect.php';
    
    $sql="SELECT max(id) FROM user";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($last_id);
    $stmt->fetch();
    if ($last_id === NULL) {
      $last_id = 0;
    }
    
    function thainum($num){  
      return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),  
      array( "o" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),  $num);  
    };  

?>

<!DOCTYPE HTML>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.1.1.min.js"></script>
    
    <title>1 คน 1 Pixel 1 ความดี เพื่อถวายแด่ในหลวงของเรา</title>
    <meta property="og:image" content="http://www.dofordad.com/main.jpg" />
  </head>
  <body>
    <script>
      var lastId = <?php echo $last_id; ?>;
      
      // -------------- Facebook -----------
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          submit(response);
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          $('#submit').show();
          $('#modal-loading').hide();
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
        }
      }
      
      function firstLoadCallBack(response) {
        console.log('firstLoadCallBack');
        console.log(response);
        if (response.status === 'connected') {
          checkStatus(response);
        } else if (response.status === 'not_authorized') {
        } else {
        }
      }
    
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1861416167413030',
          cookie     : true,  // enable cookies to allow the server to access 
                              // the session
          xfbml      : true,  // parse social plugins on this page
          version    : 'v2.8' // use graph api version 2.5
        });
      
        FB.getLoginStatus(function(response) {
          firstLoadCallBack(response);
        });
    
      };
    
      // Load the SDK asynchronously
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
      
      // -------------- Functions -----------
      
      function checkStatus(response) {
        var fbID = response.authResponse.userID;
        console.log("checkStatus: " + fbID);
        $.post( "check_status.php", { 'fb_id':  fbID}, function( data ) {
          console.log(data);
          if (data.id) {
            $('#pay-respect-button').hide();
            $('#your-pixel').html('คุณอยู่ใน pixel ที่ '+ data.id + '(จากมุมซ้ายบน) - วางเม้าส์บนpixelเพื่อดูข้อความ');
            $('#your-pixel').show();
          }
        }, "json"); 
      }
      
      function login() {
        if ($('#goal').val().length > 0) {
          $('#submit').hide();
          $('#modal-loading').show();
          console.log("star fb login");
          FB.login(function(response){
            console.log("login complete. start call back");
            statusChangeCallback(response);
          }); 
        }
      }
      
      function submit(response) {
        var accessToken = response.authResponse.accessToken;
        var goal = $('#goal').val();
        console.log("submit: " + accessToken + " " + goal);
        $.post( "submit.php", { 'fb_token':  accessToken, 'goal': goal}, function( data ) {
          console.log(data);
          location.reload();
        }, "json").fail( function(xhr, textStatus, errorThrown) {
          console.log(xhr.statusText);
          console.log(textStatus);
          console.log(error);
        }); 
      }
      
    </script>
    
    <div id="container">
      <canvas id="canvas" width="1001" height="999"></canvas>
      <canvas id="zoom-canvas" width="200" height="100"></canvas>
      <div id="tooltip">
        <img id="tooltip-loading" src="loading.gif">
        <div id="tooltip-loading-pixel"></div>
        <img id="tooltip-image" src="">
        <div id="tooltip-right">
          <div id="tooltip-title"><span id="tooltip-name"> </span><span id="tooltip-pixel"></span></div>
          <div id="tooltip-goal"></div>
        </div>
        
      </div>
    </div>
    
    <div id="pay-respect-button" class="big_buttons" onclick=showModal();>
      <a class="button gray" href="#">ร่วมถวายความอาลัย</a>
    </div>
    <div id="your-pixel">
      pixel ของคุณคือ
    </div>
    <div id="total-participant">
      มีคนร่วมแล้ว <?php echo thainum($last_id);?> / ๙๙๙,๙๙๙ คน(pixel)
    </div>
    
    <!-- The Modal -->
    <div id="myModal" class="modal">
    
      <!-- Modal content -->
      <div class="modal-content">

          <form class="form" id="form1">
            <p>ข้าพเจ้าสัญญาว่าจะ...</p>
            <p class="target">
              <textarea class="textbox" type="text" placeholder="ทำอะไร..." id="goal" maxlength="100"></textarea>
            </p>
            <p>เพื่อเป็นความดีถวายแด่พ่อของเรา</p>
            <div id="submit" class="small_buttons" onclick="login()">
              <a class="button gray" href="#">ถวายความอาลัย</a>
            </div>
            <div id="modal-loading">
              <img src='loading.gif' width=25/>
            </div>
            <p>(เมื่อลงนามแล้ว 1 pixelของภาพจะถูกเปลี่ยนจากสีเทาเป็นสีเหลือง)</p>
          </form>
      </div>
    
    </div>
    
    
    <script>
      
      // -------------- Draw canvas -----------
      function drawImage(imageObj) {
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        var x = 0;
        var y = 0;

        context.drawImage(imageObj, x, y);

        var imageData = context.getImageData(x, y, imageObj.width, imageObj.height);
        var data = imageData.data;
        
        console.log("image legnth:"+data.length);
        console.log("lastId:"+lastId);
        for(var i = 0; i < data.length; i += 4) {
          var pixel = 1 + i/4;
          if (pixel > lastId) {
            var brightness = 0.34 * data[i] + 0.5 * data[i + 1] + 0.16 * data[i + 2];
            // red
            data[i] = brightness;
            // green
            data[i + 1] = brightness;
            // blue
            data[i + 2] = brightness;
          }
        }

        // overwrite original image
        context.putImageData(imageData, x, y);
      }
      
      var imageObj = new Image();
      imageObj.onload = function() {
        drawImage(this);
      };
      imageObj.src = 'main.jpg';
      
      // -------------- Modal form -----------
      var modal = document.getElementById('myModal');
      window.onclick = function(event) {
          if (event.target == modal) {
              modal.style.display = "none";
          }
      }
      
      function showModal() {
        modal.style.display = "block";
      }
      
      // -------------- Mouse over canvas -----------
      var lastLoadingPixel;
      var isLoading = false;
      $(document).ready(function() {
         var container = $('#container').get(0);
         var canvas = $('#canvas').get(0);
         var ctx = canvas.getContext('2d');
         var zoom = document.getElementById("zoom-canvas");
         var zoomCtx = zoom.getContext("2d");
        //console.log("width:" + canvas.width);
        //console.log("height:" + canvas.height);
        canvas.addEventListener('mousemove', function(e) {
          var x = (e.pageX - container.offsetLeft);
          var y = (e.pageY - container.offsetTop);
          
          // zoom
          var zoomCanvasWidth = zoom.offsetWidth;
          var zoomCanvasHeight = zoom.offsetHeight;
          var zoomStartX = x - zoomCanvasWidth/2;
          var zoomStartY = y - zoomCanvasHeight/2;
          var multiplier = 2;
          var zoomWidth = zoomCanvasWidth*multiplier;
          var zoomHeight = zoomCanvasHeight*multiplier;
          var zoomOffsetX = -(zoomWidth - zoomCanvasWidth) / 2;
          var zoomOffsetY = -(zoomHeight - zoomCanvasHeight) / 2;
          zoomCtx.clearRect(0, 0, zoomCanvasWidth, zoomCanvasHeight);
          zoomCtx.drawImage(canvas, zoomStartX, zoomStartY, zoomCanvasWidth, zoomCanvasHeight, zoomOffsetX, zoomOffsetY, zoomWidth, zoomHeight);
          zoom.style.top = (y + 10) + "px";
          zoom.style.left = (x - zoomCanvasWidth - 10) + "px";
          // draw crosshair
          var crossHairSize = 20;
          var crossHairStartX = zoomCanvasWidth/2-crossHairSize/2
          var crossHairStartY = zoomCanvasHeight/2-crossHairSize/2
          zoomCtx.beginPath();
          zoomCtx.moveTo(crossHairStartX,zoomCanvasHeight/2+1); // horizontal line
          zoomCtx.lineTo(crossHairStartX+crossHairSize,zoomCanvasHeight/2+1);
          zoomCtx.moveTo(zoomCanvasWidth/2+1,crossHairStartY); // vertical line
          zoomCtx.lineTo(zoomCanvasWidth/2+1,crossHairStartY+crossHairSize);
          zoomCtx.strokeStyle = 'rgba(0,0,0,0.4)';
          zoomCtx.stroke();
          
          // tooltip
          
          //console.log('pageX:'+e.pageX+' e.pageY:'+e.pageY);
          //console.log('container.offsetLeft:'+container.offsetLeft+' container.offsetTop:'+container.offsetTop);
          //console.log('x:'+x+' y:'+y);
          var pixel = (y * canvas.width) + (x + 1);
          if (x >= 0 && pixel <= lastId) {
            lastLoadingPixel = pixel;
            var left = x + 20;
            var top = y + 20;
            $('#tooltip').css('left', left+'px');
            $('#tooltip').css('top', top+'px');
            $('#tooltip-loading').show();
            $('#tooltip-loading-pixel').show();
            $('#tooltip-image').hide();
            $('#tooltip-right').hide();
            $('#tooltip').show();
            $('#tooltip-loading-pixel').html(' ('+pixel+')');
            
            // load person info
            setTimeout(function(){
              if (lastLoadingPixel == pixel && !isLoading) {
                isLoading = true;
                $.post( "get_info.php", { 'pixel':  pixel}, function( data ) {
                  isLoading = false;
                  console.log(data);
                  if (data.fb_id) {
                    $('#tooltip-image').attr("src", "https://graph.facebook.com/"+data.fb_id+"/picture?type=square");
                    $('#tooltip-name').html(data.name);
                    $('#tooltip-pixel').html(' ('+pixel+')');
                    $('#tooltip-goal').html('"'+data.goal+'"');
                    $('#tooltip-loading').hide();
                    $('#tooltip-loading-pixel').hide();
                    $('#tooltip-image').show();
                    $('#tooltip-right').show();
                  } 
                },  "json");
              } else {
                 console.log("mouse is moved before loading start or loading is in progress");
              }
            },1000); 

            
          } else {
            $('#tooltip').hide();
          }
          
        }, 0);
      });
      

    </script>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-85779666-1', 'auto');
      ga('send', 'pageview');
    
    </script>
  </body>
</html>      