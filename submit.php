<?php
    $fb_token = $_POST['fb_token'];
    $json_token = file_get_contents("https://graph.facebook.com/me?access_token=$fb_token&fields=id,name");
    $json_token =json_decode($json_token);
    $fb_id = $json_token->id;
    $fb_name = $json_token->name;
    
    include 'connect.php';
    $goal =  $_POST['goal'];
    $sql="INSERT INTO user (fb_id, name, goal) VALUES (?,?,?)";
    
    $stmt = $conn->prepare($sql);
    $result = array();
    if(!$stmt ){
        $result['success'] = 0;
        $result['error'] = $conn->error;
    }  else {
        $stmt->bind_param('iss', $fb_id, $fb_name, $goal);
        if ($stmt->execute()) {
            $result['success'] = 1;
        } else {
            $result['success'] = 0;
            $result['error'] = $conn->error;
        }
    }
    
    
    echo json_encode($result);
    
?>